type Gender = 'fille' | 'garçon';

export interface Person {
  id: number;
  name: string;
  sex: Gender;
  isPresent: boolean;
}
export const PERSONS: Person[] = [
  {
    id: 1,
    name: 'eleve1',
    sex: 'fille',
    isPresent: true,
  },
  {
    id: 2,
    name: 'eleve2',
    sex: 'fille',
    isPresent: false,
  },
  {
    id: 3,
    name: 'eleve1',
    sex: 'garçon',
    isPresent: true,
  },
  {
    id: 4,
    name: 'eleve4',
    sex: 'garçon',
    isPresent: true,
  },
  {
    id: 5,
    name: 'eleve5',
    sex: 'fille',
    isPresent: false,
  },
];
