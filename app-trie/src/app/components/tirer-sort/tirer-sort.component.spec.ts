import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TirerSortComponent } from './tirer-sort.component';

describe('TirerSortComponent', () => {
  let component: TirerSortComponent;
  let fixture: ComponentFixture<TirerSortComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TirerSortComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TirerSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
