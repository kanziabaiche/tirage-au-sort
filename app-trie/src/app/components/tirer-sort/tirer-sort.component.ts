import { Component } from '@angular/core';
import { Person, PERSONS } from 'src/app/mocks/person.mock';

@Component({
  selector: 'app-tirer-sort',
  templateUrl: './tirer-sort.component.html',
  styleUrls: ['./tirer-sort.component.css'],
})
export class TirerSortComponent {
  personnes: Person[] = PERSONS;
  indexEleveSort: number = -1;
  isSelected = false;
  elevesPresent: Person[] = [];
  stockSort: Person[] = [];
  index: number = -1;
  presentEleves(): Person[] {
    this.personnes.forEach((element) => {
      const foundElement = this.elevesPresent.find((ele) => ele == element);
      if (element.isPresent && foundElement == null) {
        this.elevesPresent.push(element);
      }
    });

    return this.elevesPresent;
  }
  presentLastSort = this.presentEleves().filter(
    (ele) => ele != this.elevesPresent[this.indexEleveSort]
  );

  sort() {
    if (this.presentEleves() != null && this.presentLastSort.length >= 1) {
      console.log('present', this.presentLastSort);

      this.index = Math.floor(Math.random() * this.presentLastSort.length);
      this.isSelected = true;
      console.log('indexPresent', this.index);

      this.indexEleveSort = this.presentEleves().findIndex(
        (element) => element == this.presentLastSort[this.index]
      );

      this.presentLastSort = this.presentLastSort.filter(
        (ele) => ele != this.elevesPresent[this.indexEleveSort]
      );
    } else if (this.presentLastSort.length <= 1) {
      this.indexEleveSort = Math.floor(
        Math.random() * this.presentEleves().length
      );
      this.isSelected = true;
    }
  }
}
